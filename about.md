---
layout: page
title: About
permalink: /about/
---

This is xian's development progress page. Right now, the focus is on the video game City Night.

City Night is a video game written in C for a variety of platforms. It is a video game set in the city designed to test the player's resourcefulness and challenge the player's puzzle-solving skills. It is about a child who ventures from their home to live off of their own means. They run into trouble and so they must find out how to survive in the harsh cold of winter.